--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: catalog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalog (
    id integer NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    payload text NOT NULL,
    role integer NOT NULL,
    owner text,
    status boolean NOT NULL,
    updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    tags text[]
);


ALTER TABLE public.catalog OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id character varying(128) NOT NULL,
    token character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    state integer NOT NULL,
    role integer NOT NULL,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: catalog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.catalog (id, title, description, payload, role, owner, status, updated, created, tags) FROM stdin;
1	Location1	FakeDescription	{"latitude":"41.709829", "longitude":"-2.860610"}	0		t	2019-04-05 19:43:02.449639	2019-04-05 19:43:02.449639	{Tag1,Tag2}
2	Location2	FakeDescription 2	{"latitude":"1.709829", "longitude":"-22.860610"}	0		t	2019-04-05 19:43:02.452882	2019-04-05 19:43:02.452882	{Tag1,Tag3}
3	Location3	FakeDescription 3	{"latitude":"41.709829", "longitude":"-2.860610"}	0		t	2019-04-05 19:43:02.454004	2019-04-05 19:43:02.454004	{Tag1,Tag2}
4	Location4	FakeDescription 4	{"latitude":"1.709829", "longitude":"-22.860610"}	0		t	2019-04-05 19:43:02.455117	2019-04-05 19:43:02.455117	{Tag1,Tag3}
5	Location5	FakeDescription 5	{"latitude":"41.709829", "longitude":"-2.860610"}	1		t	2019-04-05 19:43:02.456144	2019-04-05 19:43:02.456144	{Tag1,Tag2}
6	Location6	FakeDescription 6	{"latitude":"1.709829", "longitude":"-22.860610"}	2		t	2019-04-05 19:43:02.457291	2019-04-05 19:43:02.457291	{Tag1,Tag3}
7	Location7	FakeDescription 7	{"latitude":"41.709829", "longitude":"-2.860610"}	2		t	2019-04-05 19:43:02.458893	2019-04-05 19:43:02.458893	{Tag1,Tag2}
8	Premium1	FakeDescription 8	{"latitude":"1.709829", "longitude":"-22.860610"}	3		t	2019-04-05 19:43:02.460692	2019-04-05 19:43:02.460692	{Tag1,Tag3}
9	Premium2	FakeDescription 9	{"latitude":"41.709829", "longitude":"-2.860610"}	3		t	2019-04-05 19:43:02.46253	2019-04-05 19:43:02.46253	{Tag1,Tag2}
10	Premium3	FakeDescription 10	{"latitude":"1.709829", "longitude":"-22.860610"}	3		t	2019-04-05 19:43:02.464352	2019-04-05 19:43:02.464352	{Tag1,Tag3}
11	Custom1	FakeDescription	{"latitude":"1.709829", "longitude":"-22.860610"}	0	64758166-37e6-4aa5-a7a7-7ef57f26634d	f	2019-04-05 19:43:02.465857	2019-04-05 19:43:02.465857	{Tag1,Tag3}
12	Custom2	FakeDescription	{"latitude":"41.709829", "longitude":"-2.860610"}	0	64758166-37e6-4aa5-a7a7-7ef57f26634d	f	2019-04-05 19:43:02.467074	2019-04-05 19:43:02.467074	{Tag1,Tag2}
13	Custom3	FakeDescription	{"latitude":"13.709829", "longitude":"22.860610"}	0	64758166-37e6-4aa5-a7a7-7ef57f26634d	f	2019-04-05 19:43:02.468214	2019-04-05 19:43:02.468214	{Tag1,Tag3}
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, token, username, state, role, created) FROM stdin;
af2acb39-5f44-4e1b-a045-41988f9b94df	HashedPass	fooName	0	5	2019-04-05 19:42:39.297258
ff2acb39-5f44-4e1b-a045-41988f9b94df	HashedPass	fooName	0	1	2019-04-05 19:42:39.299869
\.


--
-- Name: catalog catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalog
    ADD CONSTRAINT catalog_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: owner_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX owner_idx ON public.catalog USING hash (owner);


--
-- Name: role_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX role_idx ON public.catalog USING hash (role);


--
-- Name: tag_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tag_idx ON public.catalog USING gin (tags);


--
-- Name: user_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_id_idx ON public.users USING btree (id);


--
-- PostgreSQL database dump complete
--

