package log

import (
	"github.com/davecgh/go-spew/spew"
	"testing"
)

func TestGetMyIp(t *testing.T) {
	ip, err := ExternalIP()
	if err != nil {
		t.Errorf("Unexpected error, err %v", err)
	}

	spew.Dump(ip)
}
