# common repository

Protocol Buffer compile:
```
protoc -I . --go_out=plugins=grpc:. ./*.proto
```

Dockerfile wraps postgres adding dump fixtures

docker-compose up --remove-orphans

pg_dump -h localhost -U postgres open-cosmos > dump.sql

docker-compose up --scale front-loop=3 --remove-orphans