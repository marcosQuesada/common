package db

import (
	"fmt"
	_ "github.com/golang-migrate/migrate/database/postgres"
	"github.com/jmoiron/sqlx"
	"bitbucket.org/marcosQuesada/common/config"
	"time"
)

// NewPostgres creates a new pool connection to the database
func NewPostgres(c config.Postgres) (*sqlx.DB, error) {
	dataSource := fmt.Sprintf("user=%v password=%v dbname=%v host=%v port=%v sslmode=disable", c.Username, c.Password, c.Database, c.Host, c.Port)
	db, err := sqlx.Open("postgres", dataSource)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(c.MaxConnections)
	db.SetConnMaxLifetime(time.Duration(c.MaxConnLifetime) * time.Second)

	return db, nil
}
