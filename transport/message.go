package transport

type MessageType string

type Message interface {
	GetType() MessageType
}


