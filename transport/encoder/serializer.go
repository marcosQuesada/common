package encoder

import (
	"encoding/json"
)

type Serializer interface {
	Encode(message interface{}) ([]byte, error)
	Decode(payload []byte, raw interface{}) error
}

type JSONSerializer struct{}

func NewJSONSerializer() *JSONSerializer{
	return &JSONSerializer{}
}

func (s *JSONSerializer) Encode(message interface{}) ([]byte, error) {
	return json.Marshal(message)
}

func (s *JSONSerializer) Decode(payload []byte, msg interface{}) error {
	return json.Unmarshal(payload, msg)
}
