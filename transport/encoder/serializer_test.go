package encoder

import (
	"testing"
)

type foo struct {
	Page int      `json:"page"`
	Bar  []string `json:"bar"`
}

func TestJsonEncodeDecode(t *testing.T) {
	f := &foo{
		Page: 1,
		Bar:  []string{"foo", "bar"},
	}

	j := NewJSONSerializer()

	raw, err := j.Encode(f)
	if err != nil {
		t.Errorf("Unexpected error, err %v", err)
	}

	ff := &foo{}
	err = j.Decode(raw, ff)
	if err != nil {
		t.Errorf("Unexpected error, err %v", err)
	}

	if ff.Page != 1 {
		t.Error("Unexpected result")
	}
}

func TestJsonEncodeDecodeFromMap(t *testing.T) {
	f := map[string]string{
		"page": "1",
		"bar":  "foo",
	}

	j := NewJSONSerializer()

	raw, err := j.Encode(f)
	if err != nil {
		t.Errorf("Unexpected error, err %v", err)
	}

	ff := map[string]string{}
	err = j.Decode(raw, &ff)
	if err != nil {
		t.Errorf("Unexpected error, err %v", err)
	}

	v, ok := ff["page"]
	if !ok {
		t.Fatal("Unexpected values")
	}

	if v != "1" {
		t.Error("Unexpected result")
	}
}
