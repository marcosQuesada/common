package client

import (
	"context"
	"errors"
	proto "bitbucket.org/marcosQuesada/common/transport/grpc/content"
	domain "bitbucket.org/marcosQuesada/content-service/pkg/domain/content"
	convert "bitbucket.org/marcosQuesada/content-service/pkg/transport/grpc/handler"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"time"
)

var timeout = time.Second * 30
var ErrNotSuccess = errors.New("command unsuccessful")

type Client interface {
	GetPublicElements(ctx context.Context, role int) (domain.Catalog, error)
	GetPrivateElementByID(ctx context.Context, callerUid string, ID int) (*domain.Element, error)
	GetPrivateElementsByOwner(ctx context.Context, callerUid string) (domain.Catalog, error)
	CreatePrivateElement(ctx context.Context, element *domain.Element) error
	UpdatePrivateElement(ctx context.Context, callerUid string, element *domain.Element) error
	DeletePrivateElement(ctx context.Context,  uid string, ID int64) error
}

type GrpcClient struct {
	client proto.ContentServiceClient
	conn   *grpc.ClientConn
}

func NewClient(endpoint string) (*GrpcClient, error) {
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Errorf("did not connect: %v", err)
		return nil, err
	}

	return &GrpcClient{
		client: proto.NewContentServiceClient(conn),
		conn:   conn,
	}, nil
}

func (f *GrpcClient) GetPublicElements(ctx context.Context, role int) (domain.Catalog, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.GetPublicElements(ctx, &proto.PublicRequest{CallerRole: int64(role)})
	if err != nil {
		return nil, err
	}

	elements := []*domain.Element{}
	for _, e := range res.Elements {
		elements = append(elements, convert.ConvertFromProtocol(e))
	}
	return elements, nil
}

func (f *GrpcClient) GetPrivateElementByID(ctx context.Context, callerUid string, ID int) (*domain.Element, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.GetPrivateElementByID(ctx, &proto.ElementRequest{CallerUid: callerUid, ID: int64(ID)})
	if err != nil {
		return nil, err
	}

	return convert.ConvertFromProtocol(res), nil
}

func (f *GrpcClient) GetPrivateElementsByOwner(ctx context.Context, callerUid string) (domain.Catalog, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.GetPrivateElementsByOwner(ctx, &proto.ElementRequest{CallerUid: callerUid})
	if err != nil {
		return nil, err
	}

	elements := []*domain.Element{}
	for _, e := range res.Elements {
		elements = append(elements, convert.ConvertFromProtocol(e))
	}
	return elements, nil
}

func (f *GrpcClient) CreatePrivateElement(ctx context.Context, element *domain.Element) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.CreatePrivateElement(ctx, convert.ConvertFromDomain(element))
	if err != nil {
		return err
	}

	if !res.Success {
		return ErrNotSuccess
	}

	return nil
}

func (f *GrpcClient) UpdatePrivateElement(ctx context.Context, callerUid string, element *domain.Element) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.UpdatePrivateElement(ctx, &proto.UpdateElementRequest{CallerUid:callerUid, Element:convert.ConvertFromDomain(element)})
	if err != nil {
		return err
	}

	if !res.Success {
		return ErrNotSuccess
	}

	return nil
}

func (f *GrpcClient) DeletePrivateElement(ctx context.Context,  uid string, ID int64) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.DeletePrivateElement(ctx, &proto.ElementRequest{CallerUid: uid, ID: ID})
	if err != nil {
		return err
	}

	if !res.Success {
		return ErrNotSuccess
	}

	return nil
}
