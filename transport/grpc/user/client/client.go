package client

import (
	"bitbucket.org/marcosQuesada/common/transport/grpc/user"
	domain "bitbucket.org/marcosQuesada/user-service/pkg/domain/user"
	convert "bitbucket.org/marcosQuesada/user-service/pkg/transport/grpc/handler"
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"time"
)

var timeout = time.Second * 30
var ErrNotSuccess = errors.New("command unsuccessful")

type Client interface {
	GetUser(ctx context.Context, uid string) (*domain.User, error)
	GetAll(ctx context.Context) ([]*domain.User, error)
	Authenticate(ctx context.Context, uid string, token string) (bool, error)
	Register(ctx context.Context, user *domain.User) (bool, error)
	Update(ctx context.Context, user *domain.User) error
	Delete(ctx context.Context, uid string) error
	Terminate()
}

type GrpcClient struct {
	client proto.UserServiceClient
	conn   *grpc.ClientConn
}

func NewClient(endpoint string) (*GrpcClient, error) {
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Errorf("did not connect: %v", err)
		return nil, err
	}

	return &GrpcClient{
		client: proto.NewUserServiceClient(conn),
		conn:   conn,
	}, nil
}

func (f *GrpcClient) GetUser(ctx context.Context, ID string) (*domain.User, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.GetUser(ctx, &proto.UserRequest{ID: ID})
	if err != nil {
		return nil, err
	}

	if !res.Success {
		return nil, ErrNotSuccess
	}

	return convert.ConvertFromProtocol(res.User), nil
}

func (f *GrpcClient) GetAll(ctx context.Context) ([]*domain.User, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.GetAll(ctx, &proto.AllUserRequest{})
	if err != nil {
		return nil, err
	}

	if !res.Success {
		return nil, ErrNotSuccess
	}

	users := []*domain.User{}
	for _, u := range res.Users {
		users = append(users, convert.ConvertFromProtocol(u))
	}

	return users, nil
}

func (f *GrpcClient) Authenticate(ctx context.Context, id string, token string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.Authenticate(ctx, &proto.AuthRequest{ID: id, Token: token})
	if err != nil {
		return false, err
	}

	return res.Success, nil
}

func (f *GrpcClient) Register(ctx context.Context, user *domain.User) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	res, err := f.client.Register(ctx, &proto.RegisterRequest{User: convert.ConvertFromDomain(user)})
	if err != nil {
		return false, err
	}

	return res.Success, nil
}

func (f *GrpcClient) Update(ctx context.Context, user *domain.User) error {

	res, err := f.client.Update(ctx, &proto.UpdateRequest{User: convert.ConvertFromDomain(user)})
	if err != nil {
		return err
	}

	if !res.Success {
		return ErrNotSuccess
	}

	return nil
}

func (f *GrpcClient) Delete(ctx context.Context, UID string) error {
	res, err := f.client.Delete(ctx, &proto.UserRequest{ID: UID})
	if err != nil {
		return err
	}

	if !res.Success {
		return ErrNotSuccess
	}

	return nil
}

func (f *GrpcClient) Terminate() {
	f.conn.Close()
}
