package config

type Postgres struct {
	Host            string `config:"host"`
	Port            int    `config:"port"`
	Username        string `config:"username"`
	Password        string `config:"password"`
	Database        string `config:"database"`
	MaxConnections  int    `config:"max_connections"`
	MaxConnLifetime int    `config:"max_conn_lifetime"`
}

type Redis struct {
	Hosts     []string `yaml:"hosts"`
	Database  int      `yaml:"database"`
}

type Grpc struct {
	Port int `yaml:"port"`
}

type Http struct {
	Port int `yaml:"port"`
}